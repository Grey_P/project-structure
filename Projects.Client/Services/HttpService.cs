﻿using Newtonsoft.Json;
using Projects.Client.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Projects.Client.Services
{
    class HttpService
    {
        private string PATH = "https://localhost:5001/api";


        // 1
        public async void GetNumOfTaskByAuthorID()
        {
            try
            {
                Console.WriteLine("Введiть id Користувача(число):");
                Console.Write("--> ");
                int id = int.Parse(Console.ReadLine());
                var url = PATH + "/Projects/GetNumOfTaskByAuthorID/" + id;
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = await client.GetAsync(url);
                    Console.WriteLine("Status:" + response.StatusCode);
                    Console.WriteLine("|Project id : Task Number|");
                    Console.WriteLine("Result:" + response.Content.ReadAsStringAsync().Result);
                }
            } catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 2
        public async void GetListOfTaskForPerformer()
        {
            try
            {
                Console.WriteLine("Введiть id Користувача(число):");
                Console.Write("--> ");
                int id = int.Parse(Console.ReadLine());
                var url = PATH + "/Tasks/" + id;
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = await client.GetAsync(url);
                    Console.WriteLine("Status:" + response.StatusCode);
                    Console.WriteLine("Task id: description");
                    string json = response.Content.ReadAsStringAsync().Result + "";
                    List<TaskModel> usersTasks = JsonConvert.DeserializeObject<List<TaskModel>>(json);
                    foreach (var task in usersTasks)
                    {
                        Console.WriteLine(task.Id + ": " + task.Description);
                    }
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 3
        public async void GetFinishedTaskByUserID()
        {
            try{
                Console.WriteLine("Введiть id Користувача(число):");
                Console.Write("--> ");
                int id = int.Parse(Console.ReadLine());
                var url = PATH + "/Tasks/Finished/" + id;
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = await client.GetAsync(url);
                    Console.WriteLine("Status:" + response.StatusCode);
                    Console.WriteLine("Task id: name");
                    string json = response.Content.ReadAsStringAsync().Result + "";
                    List<IdNameOfTaskModel> usersTasks = JsonConvert.DeserializeObject<List<IdNameOfTaskModel>>(json);
                    if(usersTasks == null)
                    {
                        Console.WriteLine("Зроблений таскiв не знайдено");
                        return;
                    }
                    foreach (var task in usersTasks)
                    {
                        Console.WriteLine(task.Id + ": " + task.Name);
                    }
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 4

        public async void GetTeamWhereOld()
        {
            try
            {
                var url = PATH + "/Teams/GetTeamWhereOldPeople";
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = await client.GetAsync(url);
                    Console.WriteLine("Status:" + response.StatusCode);
                    Console.WriteLine("TeamID : name");
                    string json = response.Content.ReadAsStringAsync().Result + "";
                    List<TeamModel> teams = JsonConvert.DeserializeObject<List<TeamModel>>(json);
                    if (teams == null)
                    {
                        Console.WriteLine("Жодної команди не знайдено");
                        return;
                    }
                    foreach (var team in teams)
                    {
                        Console.WriteLine(team.Id + ": " + team.Name);
                    }
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 5
        public async void GetPeopleWithTask()
        {
            try
            {
                var url = PATH + "/Users/GetPeopleWithTask";
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = await client.GetAsync(url);
                    Console.WriteLine("Status:" + response.StatusCode);
                    Console.WriteLine("UserID+name : Number tasks");
                    string json = response.Content.ReadAsStringAsync().Result + "";
                    List<PeopleWithTasksModel> users = JsonConvert.DeserializeObject<List<PeopleWithTasksModel>>(json);
                    if (users == null)
                    {
                        Console.WriteLine("Жодного користувача не знайдено");
                        return;
                    }
                    foreach (var user in users)
                    {
                        Console.WriteLine("ID:" + user.User.Id+" | " + user.User.FirstName + ": " + user.Tasks.Count);
                    }
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 6
        public async void GetUserInfo()
        {
            try
            {
                Console.WriteLine("Введiть id Користувача(число):");
                Console.Write("--> ");
                int id = int.Parse(Console.ReadLine());
                var url = PATH + "/Users/GetUserInfo/" + id;
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = await client.GetAsync(url);
                    Console.WriteLine("Status:" + response.StatusCode);
                    string json = response.Content.ReadAsStringAsync().Result + "";
                    UserInfoModel userInfo = JsonConvert.DeserializeObject<UserInfoModel>(json);
                    if (userInfo == null)
                    {
                        Console.WriteLine("Жодного користувача не знайдено");
                        return;
                    }
                    else
                    {
                        Console.WriteLine("ID:" + userInfo.User.Id);
                        Console.WriteLine("Name:" + userInfo.User.FirstName + " " + userInfo.User.LastName);
                        if(userInfo.LastProject != null)
                        {
                            Console.WriteLine("Last project Name: " + userInfo.LastProject.Name);
                        }
                        if(userInfo.LongestTask != null)
                        {
                            Console.WriteLine("Longest task name: " + userInfo.LongestTask.Name);
                        }
                    }
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }

        // 7
        public async void GetProjectInfo()
        {
            try
            {
                var url = PATH + "/Projects/GetProjectInfo";
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = await client.GetAsync(url);
                    Console.WriteLine("Status:" + response.StatusCode);
                    string json = response.Content.ReadAsStringAsync().Result + "";
                    List<ProjectInfoModel> projectsInfo = JsonConvert.DeserializeObject<List<ProjectInfoModel>>(json);
                    if (projectsInfo == null)
                    {
                        Console.WriteLine("Жодного проекту не знайдено");
                        return;
                    }
                    else
                    {
                        foreach(var proj in projectsInfo)
                        {
                            Console.WriteLine("Назва та id: " + proj.Project.Name + "_" + proj.Project.Id );
                            Console.WriteLine("Кiлькiсть користувачiв" + proj.UserNumber);
                            if(proj.ShorterTask != null)
                            {
                                Console.WriteLine("Найкоротший таск" + proj.ShorterTask.Name);
                            }
                            if(proj.LongestTask != null)
                            {
                                Console.WriteLine("Найдовший таск" + proj.LongestTask.Name);
                            }
                            Console.WriteLine("-----------------");
                        }
                    }
                }
            }
            catch
            {
                Console.WriteLine("Неправильно введенi данi.");
            }
        }


        //********** Part for bypassing the certificate :)
        public HttpClientHandler BypassCertificate()
        {

            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            return clientHandler;

        }
        //***************************************************
    }
}
