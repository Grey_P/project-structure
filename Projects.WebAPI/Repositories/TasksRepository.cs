﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Projects.WebAPI.Interfaces;
using Projects.WebAPI.Models;
using Projects.WebAPI.DataStorage;
using Newtonsoft.Json;

namespace Projects.WebAPI.Repositories
{
    public class TasksRepository : IRepository<TaskModel>
    {
        private IEnumerable<TaskModel> _tasks;

        public TasksRepository()
        {
            string path = PathsToData.PathToTasksJSON;
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                _tasks = JsonConvert.DeserializeObject<IEnumerable<TaskModel>>(json);
            }
        }

        public void Add(TaskModel elem)
        {
            _tasks.Append(elem);
            SaveChanges();
        }

        public void Delete(int elemid)
        {
            _tasks = _tasks.Where(el => el.Id != elemid).ToList();
            SaveChanges();
        }

        public IEnumerable<TaskModel> GetAll()
        {
            return _tasks;
        }

        public void SaveChanges()
        {
            // Some magic that send data to db
        }

        public void Update(TaskModel elem)
        {
            _tasks = (_tasks.Where(el => el.Id != elem.Id).ToList()).Append(elem);
            SaveChanges();
        }
    }
}
