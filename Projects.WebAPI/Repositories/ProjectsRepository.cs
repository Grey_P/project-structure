﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Projects.WebAPI.Models;
using Projects.WebAPI.DataStorage;
using Projects.WebAPI.Interfaces;

namespace Projects.WebAPI.Repositories
{
    public class ProjectsRepository: IRepository<ProjectModel>
    {
        private IEnumerable<ProjectModel> _projects;
 
        public ProjectsRepository()
        {
            //--NOTE: With db data-initialisation could be change
            string path = PathsToData.PathToProjectsJSON;
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                _projects = JsonConvert.DeserializeObject<IEnumerable<ProjectModel>>(json);
            }
        }

        public void Add(ProjectModel elem)
        {
            _projects.Append(elem);
            SaveChanges();
        }

        public void Delete(int elemId)
        {
            _projects = _projects.Where(el => el.Id != elemId).ToList();
            SaveChanges();
        }

        public IEnumerable<ProjectModel> GetAll()
        {
            return _projects;
        }

        public ProjectModel GetByID(int id)
        {
            return (ProjectModel)_projects.Where(i => i.Id == id);
        }

        public void SaveChanges()
        {
            // Some magic that send data to db
        }

        public void Update(ProjectModel elem)
        {
            _projects = (_projects.Where(el => el.Id != elem.Id).ToList()).Append(elem);
            SaveChanges();
        }
    }
}
