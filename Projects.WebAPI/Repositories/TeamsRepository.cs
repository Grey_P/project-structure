﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Projects.WebAPI.Interfaces;
using Projects.WebAPI.Models;
using Projects.WebAPI.DataStorage;
using Newtonsoft.Json;

namespace Projects.WebAPI.Repositories
{
    public class TeamsRepository : IRepository<TeamModel>
    {
        private IEnumerable<TeamModel> _teams;

        public TeamsRepository()
        {
            string path = PathsToData.PathToTeamsJSON;
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                _teams = JsonConvert.DeserializeObject<IEnumerable<TeamModel>>(json);
            }
        }

        public void Add(TeamModel elem)
        {
            _teams.Append(elem);
            SaveChanges();
        }

        public void Delete(int elemid)
        {
            _teams = _teams.Where(el => el.Id != elemid).ToList();
            SaveChanges();
        }

        public IEnumerable<TeamModel> GetAll()
        {
            return _teams;
        }

        public void SaveChanges()
        {
            // Some magic that send data to db
        }

        public void Update(TeamModel elem)
        {
            _teams = (_teams.Where(el => el.Id != elem.Id).ToList()).Append(elem);
            SaveChanges();
        }
    }
}
