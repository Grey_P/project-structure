﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Projects.WebAPI.Interfaces;
using Projects.WebAPI.Models;
using Projects.WebAPI.DataStorage;
using Newtonsoft.Json;


namespace Projects.WebAPI.Repositories
{
    public class UsersRepository : IRepository<UserModel>
    {
        IEnumerable<UserModel> _users;

        public UsersRepository()
        {
            string path = PathsToData.PathToUsersJSON;
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                _users = JsonConvert.DeserializeObject<IEnumerable<UserModel>>(json);
            }
        }

        public void Add(UserModel elem)
        {
            _users = _users.Append(elem);
            SaveChanges();
        }

        public void Delete(int elemid)
        {
            _users = _users.Where(el => el.Id != elemid).ToList();
            SaveChanges();
        }

        public IEnumerable<UserModel> GetAll()
        {
            return _users;
        }

        public void SaveChanges()
        {
            // Some magic that send data to db
        }

        public void Update(UserModel elem)
        {
            _users = (_users.Where(el => el.Id != elem.Id).ToList()).Append(elem);
            SaveChanges();
        }
    }
}
