using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projects.WebAPI.Services;
using Projects.WebAPI.Repositories;
using Projects.WebAPI.Interfaces;
using Projects.WebAPI.Models;

namespace Projects.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // ?I don`t need to add them to all controller. 
            // cause they live only with DataService.
            ProjectsRepository projRep = new ProjectsRepository();
            TasksRepository taskRep = new TasksRepository();
            TeamsRepository teamRep = new TeamsRepository();
            UsersRepository usersRep = new UsersRepository();

            services.AddSingleton<IRepository<ProjectModel>, ProjectsRepository>(); 
            services.AddSingleton<IRepository<TaskModel>, TasksRepository>(); 
            services.AddSingleton<IRepository<TeamModel>, TeamsRepository>(); 
            services.AddSingleton<IRepository<UserModel>, UsersRepository>(); 

            //DataService dataService = new DataService(projRep, taskRep , teamRep, usersRep);
            //-------------< Added all my services >------------
            services.AddSingleton<DataService>();
            /* services.AddSingleton(new ProjectService(dataService));
             services.AddSingleton(new UsersService(dataService));
             services.AddSingleton(new TasksService(dataService));
             services.AddSingleton(new TeamsService(dataService)); */
            services.AddSingleton<ProjectService>();
            services.AddSingleton<UsersService>();
            services.AddSingleton<TasksService>();
            services.AddSingleton<TeamsService>(); 

           //---------------------------------------------------
           services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Projects.WebAPI", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Projects.WebAPI v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
