﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projects.WebAPI.Interfaces
{
    public interface ITableService<Type>
    {
        public void Add(Type el);
        public void Delete(int elid);
        public IEnumerable<Type> Get();
        public void Update(Type el);

    }
}
