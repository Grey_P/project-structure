﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projects.WebAPI.Interfaces
{
    public interface IRepository<TypeModel>
    {  
        public IEnumerable<TypeModel> GetAll();
        public void Add(TypeModel elem);
        public void Update(TypeModel elem);
        public void Delete(int elemId);
        public void SaveChanges();
    }
}
