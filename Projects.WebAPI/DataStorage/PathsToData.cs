﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Projects.WebAPI.DataStorage
{
    public static class PathsToData
    {
        public static string PathToProjectsJSON = (new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory)).Parent.Parent.Parent.FullName + "/DataStorage/Projects.json";
        public static string PathToTasksJSON = (new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory)).Parent.Parent.Parent.FullName + "/DataStorage/Tasks.json";
        public static string PathToTeamsJSON = (new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory)).Parent.Parent.Parent.FullName + "/DataStorage/Teams.json";
        public static string PathToUsersJSON = (new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory)).Parent.Parent.Parent.FullName + "/DataStorage/Users.json";
    }
}
