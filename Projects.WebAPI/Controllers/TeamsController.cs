﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projects.WebAPI.Services;
using Projects.WebAPI.Models;
using Projects.WebAPI.Models.DTO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Projects.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        TeamsService teamsService;
        public TeamsController(TeamsService ts)
        {
            teamsService = ts;
        }

        // GET: api/<TeamsController>
        [HttpGet]
        public IEnumerable<TeamModel> Get()
        {
            return teamsService.Get();
        }

        // Add Team
        // POST api/<TeamsController>
        [HttpPost]
        public IActionResult AddTeam([FromBody] TeamDTO team)
        {
            try
            {
                TeamModel teamModel = ConverterService.TeamDTOToModel(team);
                teamsService.Add(teamModel);
                return CreatedAtAction("Team added!", teamModel.Id);
            } catch
            {
                return BadRequest("invalid input format");
            }
        }

        // PUT api/<TeamsController>/5
        [HttpPut]
        public IActionResult Put([FromBody] TeamDTO teamDTO)
        {
            try
            {
                TeamModel teamModel = ConverterService.TeamDTOToModel(teamDTO);
                teamsService.Update(teamModel);
                return Ok("Object updated");
            }
            catch
            {
                return BadRequest("invalid input format");
            }
        }

        // DELETE api/<TeamsController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                teamsService.Delete(id);
                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        // 4th task from LinQ
        // Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років,
        // відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.
        [HttpGet]
        [Route("GetTeamWhereOldPeople")]
        public IEnumerable<TeamModel> GetTeamWhereOld()
        {
            return teamsService.GetTeamPeopleOlderThen10();
        }
    }
}
